/*global someFunction Area:true*/
/*global someFunction BPsToPoints:true*/
/*global someFunction intersect:true*/
/*global someFunction isClockWise:true*/
/*global someFunction isVisible:true*/
/*global someFunction mergePoints2D:true*/
/*global someFunction PointInside:true*/
/*global someFunction PointOnSegment:true*/
/*global someFunction PointReflect:true*/
/*global someFunction SegmentCrossesContour:true*/
/*global someFunction SegmentInside:true*/
/*global someFunction THREE:true*/
/*global someFunction WindingNumber:true*/

class BranchPoint
{
    constructor(point, iAssociatedVertex, offset)
    {
        this.point = point;
        this.iAssociatedVertex = iAssociatedVertex;
        this.offset = offset;
    }
}

class LoopStack
{
    constructor(contourPts)
    {
        this.contourPts = contourPts;
        this.finished = [];
        this.stack = [];
    }

    AddPoint(V)
    {
        //stack is yet empty
       if (this.stack.length === 0)
       {
           this.stack.push(new LoopStack.Loop());
           this.stack.top().AddPt(V);
       }
       else
       {
           //when vertex V should not be added because its following edge has a non-empty intersection with some of the edges of the contour

           //when vertex V should be added because its a "stand-alone" vertex

           //when vertex V should be added and the loop finished (closed) because the following edge coincides with the starting point of the loop

           //when vertex V should be added and new loop should be opened because V coincides with one of the edges of the contour.


       }
    }
}
LoopStack.Loop = class
{
    constructor()
    {
        this.vertices = [];
        this.finished = false;
    }

    AddPt(V)
    {
        this.vertices.push(V);
    }

    startingPt()
    {
        return this.vertices[0];
    }
};

class Skeleton
{
    constructor(Prev, Current, Next)
    {
        var PrevEdge, NextEdge, Direction;

        if (Area([Prev.point, Current.point, Next.point]) > 0)
        {
            PrevEdge = Prev.point.clone().sub(Current.point).normalize();
            NextEdge = Next.point.clone().sub(Current.point).normalize();
            this.Direction = PrevEdge.clone().add(NextEdge).normalize();
            this.From = Current.point.clone();
        }
        else
        {
            //console.log("calculating skeleton of a concave vertex");
            PrevEdge = Prev.point.clone().sub(Current.point).normalize();
            NextEdge = Next.point.clone().sub(Current.point).normalize();
            this.Direction = (PrevEdge.clone().add(NextEdge)).negate().normalize();
            this.From = Current.point.clone();
        }
    }
}

class vertex
{
    constructor(point, iPrev, iNext)
    {
        this.point = point;
        this.iPrev = iPrev;
        this.iNext = iNext;
    }

    CalculateInternalAngle(contour)
    {
        var A = Area([contour[this.iPrev], this.point, contour[this.iNext]]);
        var PrevEdge = this.point.clone().sub(contour[this.iPrev]);
        var NextEdge = contour[this.iNext].clone().sub(this.point);

        if (Math.abs(A) < 100 * Number.EPSILON && Math.abs(A) > -100 * Number.EPSILON)
        {
            this.internalAngle = Math.PI;
        }
        else
        {
            this.internalAngle = Math.PI - A / Math.abs(A) * Math.acos(PrevEdge.dot(NextEdge) / (PrevEdge.length() * NextEdge.length()));
        }
    }
}

function ConcatenateContourPoints(contour)
{
    var N = contour.length;
    var Verts = [];

    Verts.push(new vertex(contour[0], N - 1, 1));
    Verts[0].CalculateInternalAngle(contour);
    for (let i = 1; i < N - 1; i++)
    {
        Verts.push(new vertex(contour[i], i - 1, i + 1));
        Verts[i].CalculateInternalAngle(contour);
    }
    Verts.push(new vertex(contour[N - 1], N - 2, 0));
    Verts[N - 1].CalculateInternalAngle(contour);

    return Verts;
}

function findInitialBranchPoints(iCurrent, contour)
{
    var N = contour.length;
    var offset;
    var a, b, c;
    var projectionPoint, BranchPointCandidate, BPApprox;
    var Offsets = [];
    var AdmissableBranchPoints = [];
    var iAssociatedVertex;
    var vertexSkeleton, mitteringDirection, BPDirection, mitteringDistance;

    vertexSkeleton = new Skeleton(contour[contour[iCurrent].iPrev], contour[iCurrent], contour[contour[iCurrent].iNext]);

    for (let i = 0; i < N; i++)
    {
        if (i === iCurrent || contour[iCurrent].iNext === i || contour[iCurrent].iPrev === contour[i].iNext)
        {
            continue;
        }
        //concave vertex and the opposing visible edge
        else if (isVisible(contour, iCurrent, i) && isVisible(contour, iCurrent, contour[i].iNext))
        {
            a = contour[i].point.y - contour[contour[i].iNext].point.y;
            b = contour[contour[i].iNext].point.x - contour[i].point.x;
            c = contour[i].point.x * (contour[contour[i].iNext].point.y - contour[i].point.y) +
                contour[i].point.y * (contour[i].point.x - contour[contour[i].iNext].point.x);

            offset = 0.5 * Math.abs(a * contour[iCurrent].point.x + b * contour[iCurrent].point.y + c) / Math.sqrt(a * a + b * b);

            if (offset < Number.EPSILON) continue;

            projectionPoint = new THREE.Vector2(
                (b * (b * contour[iCurrent].point.x - a * contour[iCurrent].point.y) - a * c) / (a * a + b * b),
                (a * (a * contour[iCurrent].point.y - b * contour[iCurrent].point.x) - b * c) / (a * a + b * b));
            BranchPointCandidate = contour[iCurrent].point.clone().multiplyScalar(0.5).addScaledVector(projectionPoint, 0.5);

            if (PointInside(contour, BranchPointCandidate) &&
                intersect(projectionPoint.clone().addScaledVector(projectionPoint.clone().sub(contour[iCurrent].point), Number.EPSILON),
                contour[iCurrent].point, contour[i].point, contour[contour[i].iNext].point) !== undefined)
            {
                iAssociatedVertex = iCurrent;
                BPDirection = projectionPoint.clone().sub(BranchPointCandidate).normalize();

                //when the opposing edge is perpendicular to the vertex axis
                if (BPDirection.x <= vertexSkeleton.Direction.x + Number.EPSILON &&
                    BPDirection.x >= vertexSkeleton.Direction.x - Number.EPSILON &&
                    BPDirection.y <= vertexSkeleton.Direction.y + Number.EPSILON &&
                    BPDirection.y >= vertexSkeleton.Direction.y - Number.EPSILON)
                {
                    mitteringDirection = new THREE.Vector2(-vertexSkeleton.Direction.y, vertexSkeleton.Direction.x);
                    mitteringDistance = offset * Math.tan((contour[iCurrent].internalAngle - Math.PI) / 4);

                    BPApprox = (BranchPointCandidate.clone()).addScaledVector(mitteringDirection, mitteringDistance);

                    if (PointInside(contour, BPApprox))
                    {
                        AdmissableBranchPoints.push(new BranchPoint(BPApprox, iAssociatedVertex, offset));
                    }

                    BPApprox = (BranchPointCandidate.clone()).addScaledVector(mitteringDirection, -mitteringDistance);

                    if (PointInside(contour, BPApprox))
                    {
                        AdmissableBranchPoints.push(new BranchPoint(BPApprox, iAssociatedVertex, offset));
                    }
                }
                else
                {
                    AdmissableBranchPoints.push(new BranchPoint(BranchPointCandidate, iAssociatedVertex, offset));
                }
            }
        }
        //concave vertex and another visible concave vertex
        //concave vertex should not be adjacent
        else if (isVisible(contour, iCurrent, i) && contour[i].internalAngle > Math.PI &&
                 SegmentInside(contour, contour[iCurrent].point, contour[i].point))
        {
            offset = 0.5 * contour[iCurrent].point.distanceTo(contour[i].point);

            if (offset < Number.EPSILON) continue;

            BranchPointCandidate = contour[iCurrent].point.clone().multiplyScalar(0.5).addScaledVector(contour[i].point, 0.5);

            if (PointInside(contour, BranchPointCandidate))
            {
                AdmissableBranchPoints.push(new BranchPoint(BranchPointCandidate, iCurrent, offset));
            }
        }
    }

    return AdmissableBranchPoints;
}

function findIntermediateBranchPoint(contour, iCurrent)
{
    var N = contour.length;
    var offset;
    var a, b, c, d, e, f;
    var intersection;
    var BranchPointCandidate;
    var V0, V1, V2, V3;
    var Skeleton0, Skeleton1;

    //consecutive vertex quadruplet (edge triplet) definition

    V0 = contour[iCurrent]; V1 = contour[(iCurrent + 1) % N];
    V2 = contour[(iCurrent + 2) % N]; V3 = contour[(iCurrent + 3) % N];

    if (V1.internalAngle < Math.PI && V2.internalAngle < Math.PI)
    {
        Skeleton0 = new Skeleton(V0, V1, V2);
        Skeleton1 = new Skeleton(V1, V2, V3);

        a = Skeleton0.Direction.y;
        b = -Skeleton0.Direction.x;
        c = Skeleton0.From.y * Skeleton0.Direction.x - Skeleton0.From.x * Skeleton0.Direction.y;
        d = Skeleton1.Direction.y;
        e = -Skeleton1.Direction.x;
        f = Skeleton1.From.y * Skeleton1.Direction.x - Skeleton1.From.x * Skeleton1.Direction.y;

        if (a * e - b * d !== 0)
        {
            intersection = new THREE.Vector2(
               (b * f - c * e) / (a * e - b * d),
               (c * d - f * a) / (a * e - b * d));

            offset = Math.abs((V2.point.y - V1.point.y) * intersection.x - (V2.point.x - V1.point.x) * intersection.y +
                               V2.point.x * V1.point.y - V2.point.y * V1.point.x) /
                               Math.sqrt((V2.point.y - V1.point.y) * (V2.point.y - V1.point.y) + (V2.point.x - V1.point.x) * (V2.point.x - V1.point.x));

            if (!SegmentCrossesContour(contour, intersection, V1.point) && !SegmentCrossesContour(contour, intersection, V2.point))
            {
                BranchPointCandidate = intersection;

                return new BranchPoint(BranchPointCandidate, contour[iCurrent].iNext, offset);
            }
        }
    }

    return undefined;
}

//a contour is nil if all of its intermediate branch points are equal
function IsNil(contour)
{
    var ContourPoints = [];

    for (let i = 0; i < contour.length; i++)
    {
        ContourPoints.push(contour[i].point);
    }
    if (Area(ContourPoints) > 0)
    {
        var intersections = [];
        var N = contour.length;
        var intermediateBP;

        for (let i = 0; i < N; i++)
        {
            if (contour[i].internalAngle > Math.PI) return false;
            intermediateBP = findIntermediateBranchPoint(contour, i);
            if (intermediateBP !== undefined) intersections.push(intermediateBP.point);
        }

        return (mergePoints2D(intersections).length === 1);
    }

    return false;
}

function MedialAxisTransform(contours, thresholdAngle)
{
    var ContourQueue = [];
    var EffectiveBPs = [];
    var CurrentContour;

    for (let i = 0; i < contours.length; i++)
    {
        ContourQueue.push(ConcatenateContourPoints(contours[i]));
    }

    while (ContourQueue.length > 0)
    {
        CurrentContour = ContourQueue.shift();
        var AdmissableBPs = [];
        var AdmissableInitialBPs = [];
        var AdmissableIntermediateBPs = [];
        var minOffset;
        var NewBPCandidates;

        if (!IsNil(CurrentContour))
        {
            for (let i = 0; i < CurrentContour.length; i++)
            {
                //Find candidates for initial branch points for reentrant concave vertices
                if (CurrentContour[i].internalAngle > Math.PI)
                {
                    NewBPCandidates = findInitialBranchPoints(i, CurrentContour);
                    if (NewBPCandidates !== undefined)
                    {
                        AdmissableBPs = AdmissableBPs.concat(NewBPCandidates);
                        AdmissableInitialBPs = AdmissableInitialBPs.concat(NewBPCandidates);
                    }
                }
                //Find candidate for intermediate branch point for a quadruplet of adjacent convex vertices
                NewBPCandidates = findIntermediateBranchPoint(CurrentContour, i);
                if (NewBPCandidates !== undefined)
                {
                    AdmissableBPs.push(NewBPCandidates);
                    AdmissableIntermediateBPs.push(NewBPCandidates);
                }
            }

            minOffset = AdmissableBPs[0].offset;
            for (let i = 1; i < AdmissableBPs.length; i++)
            {
                if (AdmissableBPs[i].offset < minOffset)
                {
                    minOffset = AdmissableBPs[i].offset;
                }
            }
            for (let i = 0; i < AdmissableBPs.length; i++)
            {
                if (AdmissableBPs[i].offset > minOffset - Number.EPSILON && AdmissableBPs[i].offset < minOffset + Number.EPSILON)
                {
                    EffectiveBPs.push(AdmissableBPs[i]);
                }
            }
        }
    }

    return [BPsToPoints(AdmissableInitialBPs),
            BPsToPoints(AdmissableIntermediateBPs),
            BPsToPoints(EffectiveBPs),
            NewContours(CurrentContour, EffectiveBPs)];
}

function NewContours (oldContour, EffectiveBPs)
{
    var N = oldContour.length;
    var AssocBPs;
    //here all new offsetted pts will be pushed
    var OffsettedContourPts = [];

    var minOffset = EffectiveBPs[0].offset;
    var a, b, d, e;
    var edgeNormal0, edgeNormal1, intersection;
    var vertexSkeleton, Direction, mitteringDistance, intermediateOffset;
    var newPt0, newPt1;
    var edgeLimit = 0.25 * minOffset;

    for (let i = 0; i < N; i++)
    {
        vertexSkeleton = new Skeleton(oldContour[oldContour[i].iPrev], oldContour[i], oldContour[oldContour[i].iNext]);

        if (oldContour[i].internalAngle < Math.PI)
        {
            AssocBPs = FindAssociatedBPs(oldContour, EffectiveBPs, i);

            if (AssocBPs.length === 0)
            {
                //this offset needs re-derivation (or get adapted to the shape)
                intermediateOffset = minOffset / Math.sin(oldContour[i].internalAngle / 2);
                OffsettedContourPts.push(oldContour[i].point.clone().addScaledVector(vertexSkeleton.Direction, intermediateOffset));
            }
            else if (AssocBPs.length === 1)
            {
                OffsettedContourPts.push(AssocBPs[0].point.clone());
            }
        }
        else if (oldContour[i].internalAngle > Math.PI)
        {
            AssocBPs = FindAssociatedBPs(oldContour, EffectiveBPs, i);

            if (AssocBPs.length === 0)
            {
                mitteringDistance = minOffset * Math.tan((oldContour[i].internalAngle - Math.PI) / 4);
                Direction = new THREE.Vector2(-vertexSkeleton.Direction.y, vertexSkeleton.Direction.x);

                newPt0 = (oldContour[i].point.clone().addScaledVector(vertexSkeleton.Direction, minOffset)).addScaledVector(Direction, mitteringDistance);
                newPt1 = (oldContour[i].point.clone().addScaledVector(vertexSkeleton.Direction, minOffset)).addScaledVector(Direction, -mitteringDistance);
            }
            else if (AssocBPs.length === 1)
            {
                newPt0 = AssocBPs[0].point.clone();
                newPt1 = PointReflect(newPt0, vertexSkeleton.Direction, vertexSkeleton.From);
            }
            else if (AssocBPs.length === 2)
            {
                newPt0 = AssocBPs[0].point.clone();
                newPt1 = AssocBPs[1].point.clone();
            }

            //if new points are too close to each other, make them identical
            if (newPt0.distanceTo(newPt1) < edgeLimit)
            {
                newPt1 = newPt0;
            }
            //else make sure the orientation is kept (if not, swap)
            else if (newPt0.distanceTo(newPt1) >= edgeLimit && !isClockWise([oldContour[i].point, newPt0, newPt1]))
            {
                newPt1 = [newPt0, newPt0 = newPt1][0];
            }

            OffsettedContourPts.push(newPt0);
            if (!newPt0.equals(newPt1)) OffsettedContourPts.push(newPt1);
        }
    }

    OffsettedContourPts = mergePoints2D(OffsettedContourPts);

    var OffsettedContour = ConcatenateContourPoints(OffsettedContourPts);

    function FindAssociatedBPs (contour, BPs, iCurrent)
    {
        var AssociatedBPs = [];

        for (let i = 0; i < BPs.length; i++)
        {
            if (BPs[i].iAssociatedVertex === iCurrent) AssociatedBPs.push(BPs[i]);
        }

        return AssociatedBPs;
    }

    return OffsettedContourPts;
}


/*global someFunction Area:true*/
/*global someFunction BoundingRectangle:true*/
/*global someFunction ConcatenateContourPoints:true*/
/*global someFunction DistanceToContour:true*/
/*global someFunction equalsWithEpsilon:true*/
/*global someFunction intersect:true*/
/*global someFunction intersectContour:true*/
/*global someFunction isClockWise:true*/
/*global someFunction isVisible:true*/
/*global someFunction mergePoints2D:true*/
/*global someFunction OrientationParam:true*/
/*global someFunction PointInside:true*/
/*global someFunction PointOnContour:true*/
/*global someFunction PointOnSegment:true*/
/*global someFunction PointReflect:true*/
/*global someFunction PushVertexGeometry:true*/
/*global someFunction SegmentCrossesContour:true*/
/*global someFunction SegmentInside:true*/
/*global someFunction Subdivide:true*/
/*global someFunction THREE:true*/
/*global someFunction Voronoi:true*/
/*global someFunction WindingNumber:true*/

class Edge
{
    constructor (V0, V1)
    {
        this.V0 = V0; this.V1 = V1;
    }

    edgeLength ()
    {
        return this.V0.distanceTo(this.V1);
    }

    edgeMidpoint ()
    {
        var lambda = 0.5;

        return this.V0.clone().multiplyScalar(1 - lambda).addScaledVector(this.V1, lambda);
    }

    edgeNormal ()
    {
        var Tangent = this.V1.clone().sub(this.V0).normalize();
        // "inward" & with respect to the origin
        return Tangent.rotateAround(new THREE.Vector2(0, 0), 0.5 * Math.PI);
    }

    AssignTriangleLeft(T)
    {
        this.triangleLeft = T;
    }

    AssignTriangleRight(T)
    {
        this.triangleRight = T;
    }
}

class Triangle
{
    constructor (V0, V1, V2)
    {
        this.V0 = V0; this.V1 = V1; this.V2 = V2;
    }

    //the following methods assign this triangle's unique edges, making this triangle their "triangles on the left"
    AssignEdge0(edge)
    {
        this.Edge0 = edge;
        this.Edge0.AssignTriangleLeft(this);
    }

    AssignEdge1(edge)
    {
        this.Edge1 = edge;
        this.Edge1.AssignTriangleLeft(this);
    }

    AssignEdge2(edge)
    {
        this.Edge2 = edge;
        this.Edge2.AssignTriangleLeft(this);
    }

    //the following methods assign this triangle's neighbors and assign the corresponding edge's triangle on the right
    AssignNeighbor0(triangle)
    {
        this.Neighbor0 = triangle;
        this.Edge0.AssignTriangleRight(this.Neighbor0);
    }

    AssignNeighbor1(triangle)
    {
        this.Neighbor1 = triangle;
        this.Edge1.AssignTriangleRight(this.Neighbor1);
    }

    AssignNeighbor2(triangle)
    {
        this.Neighbor2 = triangle;
        this.Edge2.AssignTriangleRight(this.Neighbor2);
    }
}

function AdvancingFrontTriangulate (Contour, elemSize)
{
    var Triangles = [];
    var subdivContour = Subdivide(Contour, elemSize);

    subdivContour = ConcatenateContourPoints(subdivContour);

    var N = subdivContour.length;
    var FrontEdges = [];
    var MeshNodes = [];

    //building the first iteration of the front
    for (let i = 0; i < N; i++)
    {
        MeshNodes.push(subdivContour[i].point);
        FrontEdges.push(new Edge(subdivContour[i].point, subdivContour[subdivContour[i].iNext].point));
    }

    var currentEdge;
    var iterCount = 0;

    while (FrontEdges.length > 0)
    {
        if (iterCount > 10 * N * N)
        {
            break;
        }
        else
        {
            currentEdge = FrontEdges.shift();
            AddTriangle(currentEdge);
            iterCount++;
        }
    }

    // ----------------------------------------------------------------------------------------------------
    // =============== component functions ================================================================
    // ----------------------------------------------------------------------------------------------------

    function AddTriangle (edge)
    {
        var radius = 0.7 * edge.edgeLength();
        var triangleHeight = edge.edgeLength() * Math.sqrt(3) / 2.;
        var thirdPoint = edge.edgeMidpoint().clone().addScaledVector(edge.edgeNormal(), triangleHeight);

        //check if any points of the advancing front lie within a given radius from the third point
        var inCircle = false;
        var thirdPointCandidates = [];
        var thirdPtCandidate;

        for (let i = 0; i < FrontEdges.length; i++)
        {
            if (thirdPoint.distanceTo(FrontEdges[i].V0) < radius)
            {
                inCircle = true;
                thirdPointCandidates.push(FrontEdges[i].V0);
            }
        }

        //if they do, then for all the points that lie inside the circle, choose the one with which the edge
        //forms the "highest quality" triangle
        if (inCircle)
        {
            var quality = 0;
            var newQuality;

            for (let i = 0; i < thirdPointCandidates.length; i++)
            {
                newQuality = TriangleQuality(edge.V0, edge.V1, thirdPointCandidates[i]);
                //candidates are considered as long as their quality is better as well as if the do not intersect the existing front
                if (newQuality > quality && !IntersectsFront(edge, thirdPointCandidates[i]))
                {
                    quality = newQuality;
                    thirdPtCandidate = thirdPointCandidates[i];
                }
                else
                {
                    thirdPtCandidate = thirdPointCandidates[i];
                }
            }
        }
        else
        {
            //a new ideal point is added
            MeshNodes.push(thirdPoint);
            thirdPtCandidate = thirdPoint;
        }

        //a new triangle will be added only if it does not collide with the front
        if (!IntersectsFront(edge, thirdPtCandidate) /* &&
    (PointInside(subdivContour, thirdPtCandidate) || PointOnContour(subdivContour, thirdPtCandidate)) */)
        {
            var returnTriangle = new Triangle(edge.V0, edge.V1, thirdPtCandidate);

            returnTriangle.AssignEdge0(new Edge(edge.V0, edge.V1)); //assign base edge (copy)

            //new edges init
            var newEdgeLeft, newEdgeRight;

            newEdgeLeft = new Edge(returnTriangle.V0, returnTriangle.V2);
            newEdgeRight = new Edge(returnTriangle.V1, returnTriangle.V2);

            // ================ assigning the new triangle's neighbors =============================
            // ----------------------- begin with the base edge ------------------------------------
            if (edge.triangleRight !== undefined) returnTriangle.AssignNeighbor0(edge.triangleRight);

            // ----------------------- now move on to the left edge --------------------------------
            // ----- checking if the new edge on the left isn't coincident with some of the edges in the front
            var coincidentEdge = EdgeCoincidence(newEdgeLeft);

            if (coincidentEdge !== undefined)
            {
                var coincidentEdgeId = coincidentEdge[1];
                coincidentEdge = coincidentEdge[0];

                returnTriangle.AssignEdge2(new Edge(coincidentEdge.V0, coincidentEdge.V1));
                //the new neighbor should be on the left side of the edge, but just to be sure...
                if (coincidentEdge.triangleLeft !== undefined)
                {
                    returnTriangle.AssignNeighbor2(coincidentEdge.triangleLeft);
                }
                FrontEdges.splice(coincidentEdgeId, 1); //remove left edge from the front
            }
            else
            {
                returnTriangle.AssignEdge2(newEdgeLeft);
                FrontEdges.push(new Edge(returnTriangle.V0, returnTriangle.V2)); //making sure the front is CCW oriented
            }

            // ----------------------- and finish with the right edge --------------------------------
            // ----- checking if the new edge on the right isn't coincident with some of the edges in the front
            coincidentEdge = EdgeCoincidence(newEdgeRight);

            if (coincidentEdge !== undefined)
            {
                coincidentEdgeId = coincidentEdge[1];
                coincidentEdge = coincidentEdge[0];

                returnTriangle.AssignEdge1(new Edge(returnTriangle.V0, returnTriangle.V2));
                //the new neighbor should be on the left side of the edge, but just to be sure...
                if (coincidentEdge.triangleLeft !== undefined)
                {
                    returnTriangle.AssignNeighbor1(coincidentEdge.triangleLeft);
                }
                FrontEdges.splice(coincidentEdgeId, 1); //remove right edge from the front
            }
            else
            {
                returnTriangle.AssignEdge1(newEdgeRight);
                FrontEdges.push(new Edge(returnTriangle.V0, returnTriangle.V2));
            }

            //================= By now, the triangle should be all set ========================================

            Triangles.push(returnTriangle);
        }
        // Now if no triangle seems acceptable (intersects the front), the function needs to put the edge back into the front
        else
        {
            FrontEdges.push(edge);
        }
    }

    //the pair of edges will always have opposite orientation
    function EdgeCoincidence (edge)
    {
        var epsilon = 0.0001;
        var coincidentEdge;

        for (let i = 0; i < FrontEdges.length; i++)
        {
            if (((edge.V0.x < FrontEdges[i].V1.x + epsilon && edge.V0.x > FrontEdges[i].V1.x - epsilon) &&
                (edge.V0.y < FrontEdges[i].V1.y + epsilon && edge.V0.y > FrontEdges[i].V1.y - epsilon)) &&
                ((edge.V1.x < FrontEdges[i].V0.x + epsilon && edge.V1.x > FrontEdges[i].V0.x - epsilon) &&
                (edge.V1.y < FrontEdges[i].V0.y + epsilon && edge.V1.y > FrontEdges[i].V0.y - epsilon)))
            {
                coincidentEdge = [FrontEdges[i], i];
            }
        }
        return coincidentEdge;
    }


    function IntersectsFront (edge, thirdPoint)
    {
        for (let i = 0; i < FrontEdges.length; i++)
        {
            //adjacent edges
            if (equalsWithEpsilon(edge.V1, FrontEdges[i].V0, 0.00001) ||
                equalsWithEpsilon(edge.V0, FrontEdges[i].V1, 0.00001))
            {
                continue;
            }
            if (intersect(edge.V0, thirdPoint, FrontEdges[i].V0, FrontEdges[i].V1) !== undefined ||
                intersect(edge.V1, thirdPoint, FrontEdges[i].V0, FrontEdges[i].V1) !== undefined)
            {
                return true;
            }
        }
        return false;
    }

    function MaxEdge (Pts)
    {
        var edgeLength = 0;
        var len;
        var P1, P2;

        for (let i = 0; i < Pts.length; i++)
        {
            P1 = Pts[i];
            P2 = Pts[(i + 1) % Pts.length];
            len = P1.distanceTo(P2);
            if (len > edgeLength)
            {
                edgeLength = len;
            }
        }
        return edgeLength;
    }

    // sum of 3 weights: |edge|/|maxEdge| divided by 3
    // returns 1 if V0, V1, V2 is an equilateral triangle
    function TriangleQuality (V0, V1, V2)
    {
        var E0 = V1.clone().sub(V0).length();
        var E1 = V2.clone().sub(V1).length();
        var E2 = V0.clone().sub(V2).length();
        var maxE = Math.max(E0, E1, E2);
        return (E0 + E1 + E2) / (3 * maxE);
    }
    //====================== end of additional functions =============================================

    //just for the sake of testing, I'll output a list of triplets to be displayed as faces with edges instead of
    //returning Triangle objects, because it might not work without the use of a global module. In the future, however,
    //Triangle objects are vital because they contain references to their edges and neighbors
    var VertexTriplets = []; //an array of triplets [V0, V1, V2] of Vector2 objects to be returned

    for (let i = 0; i < Triangles.length; i++)
    {
        VertexTriplets.push([Triangles[i].V0, Triangles[i].V1, Triangles[i].V2]);
    }

    return VertexTriplets;
}


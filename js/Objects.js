/*global someFunction AdvancingFrontTriangulate:true*/
/*global someFunction Area:true*/
/*global someFunction BoundingRectangle:true*/
/*global someFunction ConcatenateContourPoints:true*/
/*global someFunction DistanceToContour:true*/
/*global someFunction MedialAxisTransform:true*/
/*global someFunction PointInside:true*/
/*global someFunction polygPoints:true*/
/*global someFunction polygPoints2:true*/
/*global someFunction polygPoints3:true*/
/*global someFunction polygPoints4:true*/
/*global someFunction polygPointsHole1:true*/
/*global someFunction Subdivide:true*/
/*global someFunction THREE:true*/
/*global someFunction Voronoi:true*/
/*global someFunction VoronoiMesh:true*/

function ObjectsToScene (scene, AdvancingFront = true, quads = false, vertexLabels = false)
{
    var objects;
    var voronoi, diagram, BR, bbox;

    var boundaryElemSize = 0.25;

    var polygonPoints = polygPoints3();

    //geometries init
    var pointsBasicGeometry = new THREE.Geometry();
    var pointsNewGeometry = new THREE.Geometry();
    var polygonOutlineGeometry = new THREE.Geometry();
    var polygonOutline2Geometry = new THREE.Geometry();
    var pointsVoronoiGeometry = new THREE.Geometry();

    //point materials init
    var pointsBasicMaterial = new THREE.PointsMaterial({
		color: 0x00ff00,
		size: 0.05
    });
    var newPolygonOutlineMaterial = new THREE.LineBasicMaterial({
        color: 0xf3770f,
        linejoin: 'round',
        linewidth: 2
    });

    //line materials init
    var polygonOutlineMaterial = new THREE.LineBasicMaterial({
        color: 0x00ff00,
        linejoin: 'round',
        linewidth: 2
    });

    //face materials init
    var polygonFaceMaterial = new THREE.MeshPhongMaterial({
        color: 0x156289,
        emissive: 0x072534,
        side: THREE.DoubleSide,
        flatShading: true
    });

    //-------------------------------------------------------------------------------------------------
    // -------------- My mesh materials ---------------------------------------------------------------

    var meshOutlineMaterial = new THREE.MeshBasicMaterial({
        color: 0x000000,
        wireframe: true,
        transparent: true
    });

    var meshFaceMaterial;

    //shape geometry init
    //var polygonShape = new THREE.Shape(polygContour);
    var polygonShape = new THREE.Shape(polygonPoints);

    //polygHole = new THREE.Shape(polygHole);

    //polygonShape.holes.push(polygHole);
    var polygonGeometry = new THREE.ShapeGeometry(polygonShape);

    //============== now the we decide whether we want to triangulate using AdvancingFront or VoronoiMAT ==============

    // ==============================================================================================
    // ==================== ADVANCING FRONT TECHNIQUE (just triangles so far) =======================
    // ==============================================================================================
    if (AdvancingFront)
    {
        PushVertexGeometry(Vectors2ToVectors3(polygonPoints, true), pointsBasicGeometry);
        PushVertexGeometry(Vectors2ToVectors3(polygonPoints, true), polygonOutlineGeometry);

        //-------------------------------------------------------------------------------
        //objects
        objects = {
                polygonBasicMesh: new THREE.SceneUtils.createMultiMaterialObject(polygonGeometry, [polygonFaceMaterial]),
                polygonOutline: new THREE.Line(polygonOutlineGeometry, polygonOutlineMaterial),
                pointsBasicVerts: new THREE.Points(pointsBasicGeometry, pointsBasicMaterial)
        };

        Object.keys(objects).forEach(function(key)
        {
            scene.add(objects[key]);
        });

        //------------------------------------------------------------------------------------------------
        var AdvancingFrontTriplets = AdvancingFrontTriangulate(polygonPoints, boundaryElemSize);

        //--add new meshed Advancing Front Geometry ---------------------------------------

        // Note: I've chosen to add triangles one by one to the scene, so that I could distinguish them according to their index
        // without having to number them with vertex labels beginning:(red), end:(blue)

        for (let i = 0; i < AdvancingFrontTriplets.length; i++)
        {
            var AdvancingFrontGeometry = new THREE.Geometry();
            //vertices
            AdvancingFrontGeometry.vertices.push(Vectors2ToVectors3([AdvancingFrontTriplets[i][0]], false)[0]);
            AdvancingFrontGeometry.vertices.push(Vectors2ToVectors3([AdvancingFrontTriplets[i][1]], false)[0]);
            AdvancingFrontGeometry.vertices.push(Vectors2ToVectors3([AdvancingFrontTriplets[i][2]], false)[0]);

            meshFaceMaterial = new THREE.MeshPhongMaterial({
                color: new THREE.Color(1 - i / AdvancingFrontTriplets.length, 0.1, i / AdvancingFrontTriplets.length),
                emissive: 0x072534,
                side: THREE.DoubleSide,
                flatShading: true
            });

            /*
            if (Area(AdvancingFrontTriplets[i]) > 0)
            {
                meshFaceMaterial.color = new THREE.Color(0xed770f);
            }
            else
            {
                meshFaceMaterial.color = new THREE.Color(0x4685ea);
            }*/

            //faces
            //AdvancingFrontGeometry.faces.push(3 * i, 3 * i + 1, 3 * i + 2); //last three indices
            AdvancingFrontGeometry.faces.push(new THREE.Face3(0, 1, 2));
            AdvancingFrontGeometry.computeFaceNormals();

            scene.add(new THREE.SceneUtils.createMultiMaterialObject(AdvancingFrontGeometry, [meshFaceMaterial, meshOutlineMaterial]));
        }
    }
    // ==============================================================================================
    // ==================== VORONOI MEDIAL AXIS TRANSFORM MESH (approximation) ======================
    // ==============================================================================================
    else
    {
        polygonPoints = Subdivide(polygonPoints, boundaryElemSize);
        var voronoiBoundarySubdiv = polygonPoints;

        PushVertexGeometry(Vectors2ToVectors3(polygonPoints, true), pointsBasicGeometry);
        PushVertexGeometry(Vectors2ToVectors3(polygonPoints, true), polygonOutlineGeometry);

        var voronoiEdgeGeometries = [];

        //rhill voronoi init
        //arguments: Pts, vertices = true, edges = true, medial = false, HolePts = undefined
        VoronoiInit(polygonPoints, false, true, true);

        //---------------Trying out the new Voronoi Mesh method------------------------
        //-----------------------------------------------------------------------------

        var VoronoiMeshResult = VoronoiMesh(polygonPoints, diagram, boundaryElemSize);
        var NewPts = Vectors2ToVectors3(VoronoiMeshResult[0], false);

        PushVertexGeometry(NewPts, pointsNewGeometry);

        //-------------- additional voronoi material init ------------------------------

        var pointsNewMaterial = new THREE.PointsMaterial({
            color: 0x000000,
            size: 0.005
        });

        var pointsVoronoiMaterial = new THREE.PointsMaterial({
            color: 0xff0000,
            size: 0.05
        });

        var VoronoiEdgeMaterial = new THREE.LineBasicMaterial({
            color: 0x00ffff,
            linejoin: 'round',
            linewidth: 2
        });

        //-------------------------------------------------------------------------------
        //objects
        objects = {
                polygonBasicMesh: new THREE.SceneUtils.createMultiMaterialObject(polygonGeometry, [polygonFaceMaterial]),
                polygonOutline: new THREE.Line(polygonOutlineGeometry, polygonOutlineMaterial),
                pointsBasicVerts: new THREE.Points(pointsBasicGeometry, pointsBasicMaterial),
                pointsNewVertices: new THREE.Points(pointsNewGeometry, pointsNewMaterial),
                pointsVoronoi: new THREE.Points(pointsVoronoiGeometry, pointsVoronoiMaterial)
        };

        Object.keys(objects).forEach(function(key)
        {
        scene.add(objects[key]);
        });

        //add individual voronoi edges as lines
        for (let j = 0; j < voronoiEdgeGeometries.length; j++)
        {
            scene.add(new THREE.Line(voronoiEdgeGeometries[j], VoronoiEdgeMaterial));
        }

        //------ voronoi mesh face material ----------------------------------------------
        meshFaceMaterial = new THREE.MeshPhongMaterial({
            color: 0xed770f,
            emissive: 0x072534,
            side: THREE.DoubleSide,
            flatShading: true
        });

        if (!quads)
        {
            //---add new meshed Delaunay geometry-----------------------------------------
            var DelaunayGeometry = new THREE.Geometry();
            var TriangleIndices = VoronoiMeshResult[1];
            var i0, i1, i2;

            for (let i = 0; i < NewPts.length; i++)
            {
                DelaunayGeometry.vertices.push(NewPts[i]);
            }

            for (let i = 0; i < TriangleIndices.length; i++)
            {
                i0 = TriangleIndices[i][0];
                i1 = TriangleIndices[i][1];
                i2 = TriangleIndices[i][2];

                DelaunayGeometry.faces.push(new THREE.Face3(i0, i1, i2));
            }

            DelaunayGeometry.computeFaceNormals();

            scene.add(new THREE.SceneUtils.createMultiMaterialObject(DelaunayGeometry, [meshFaceMaterial, meshOutlineMaterial]));
        }
        else
        {
            //---add new meshed Quad geometry----------------------------------------------
            var compositeGeometries = [];
            var lineGeometry, faceGeometry;

            var faceMaterial = new THREE.MeshPhongMaterial({
                color: 0xed770f,
                emissive: 0x072534,
                side: THREE.DoubleSide,
                flatShading: true
            });
            var lineMaterial = new THREE.LineBasicMaterial({
                color: 0x000000,
                linejoin: 'round',
                linewidth: 2
            });

            var Quads = VoronoiMeshResult[2];

            for (let i = 0; i < Quads.length; i++)
            {
                lineGeometry = new THREE.Geometry();
                faceGeometry = new THREE.Geometry();

                if (Quads[i].l !== undefined)
                {
                    PushVertexGeometry(Vectors2ToVectors3(
                        [NewPts[Quads[i].i], NewPts[Quads[i].j], NewPts[Quads[i].k], NewPts[Quads[i].l], NewPts[Quads[i].i]]
                        , false, false, true), lineGeometry);
                    PushVertexGeometry(Vectors2ToVectors3(
                        [NewPts[Quads[i].i], NewPts[Quads[i].j], NewPts[Quads[i].k], NewPts[Quads[i].l]]
                        , false), faceGeometry);
                    faceGeometry.faces.push(new THREE.Face3(0, 1, 2));
                    faceGeometry.faces.push(new THREE.Face3(0, 2, 3));
                }
                else
                {
                    PushVertexGeometry(Vectors2ToVectors3(
                        [NewPts[Quads[i].i], NewPts[Quads[i].j], NewPts[Quads[i].k], NewPts[Quads[i].i]]
                        , false, false, true), lineGeometry);
                    PushVertexGeometry(Vectors2ToVectors3(
                        [NewPts[Quads[i].i], NewPts[Quads[i].j], NewPts[Quads[i].k]]
                        , false), faceGeometry);
                    faceGeometry.faces.push(new THREE.Face3(0, 1, 2));
                }

                faceGeometry.computeFaceNormals();

                scene.add(new THREE.SceneUtils.createMultiMaterialObject(faceGeometry, [faceMaterial]));
                scene.add(new THREE.Line(lineGeometry, lineMaterial));
            }
        }

        //-----------Mesh Node Numbering ----------------------------------------------
        if (vertexLabels)
        {
            var loader = new THREE.FontLoader();
            var textMeshes = [];
            var Contour1 = ConcatenateContourPoints(polygonPoints);

            for (let i = 0; i < NewPts.length; i++)
            {
                loader.load('/fonts/helvetiker_regular.typeface.json', function (font)
                {
                    var geometry = new THREE.TextGeometry(("d = " + DistanceToContour(NewPts[i], Contour1).toString()),
                    {
                        font: font,
                        size: 0.05 * boundaryElemSize,
                        height: 0,
                        curveSegments: 1,
                        bevelEnabled: false
                    });

                    var textMaterial = new THREE.MeshBasicMaterial({
                        color: 0xff0000,
                        side: THREE.DoubleSide
                    });
                    var textMesh = new THREE.Mesh(geometry, textMaterial);

                    textMesh.position.set(NewPts[i].x + 0.1 * boundaryElemSize, NewPts[i].y - 0.1 * boundaryElemSize, 0.03 + 0.1 * boundaryElemSize);

                    scene.add(textMesh);
                });
                loader.load('/fonts/helvetiker_regular.typeface.json', function (font)
                {
                    var geometry = new THREE.TextGeometry(i.toString(),
                    {
                        font: font,
                        size: 0.1 * boundaryElemSize,
                        height: 0,
                        curveSegments: 1,
                        bevelEnabled: false
                    });

                    var textMaterial = new THREE.MeshBasicMaterial({
                        color: 0xffffff,
                        side: THREE.DoubleSide
                    });
                    var textMesh = new THREE.Mesh(geometry, textMaterial);

                    textMesh.position.set(NewPts[i].x, NewPts[i].y, 0.03 + 0.1 * boundaryElemSize);

                    scene.add(textMesh);
                });
            }
        }
        //--------------- end of what happens if we choose VoronoiMAT mesh ---------------------------------
    }

    // ====== useful functions ====================================================
    //-----------------------------------------------------------------------------
    function Vectors2ToVectors3 (points, baseline, baseline2 = false, baseline3 = false)
    {
        var elevation = 0.025;
        var elevation2 = 0.05;
        var elevation3 = 0.026;
        var new3DPts = [];

        for (let i = 0; i < points.length; i++)
        {
            if (baseline)
            {
                new3DPts.push(new THREE.Vector3(points[i].x, points[i].y, 0));
            }
            else if (baseline2)
            {
                new3DPts.push(new THREE.Vector3(points[i].x, points[i].y, elevation2));
            }
            else if (baseline3)
            {
                new3DPts.push(new THREE.Vector3(points[i].x, points[i].y, elevation3));
            }
            else
            {
                new3DPts.push(new THREE.Vector3(points[i].x, points[i].y, elevation));
            }
        }

        return new3DPts;
    }

    function PushVertexGeometry (points, geometry)
    {
        for (let i = 0; i < points.length; i++)
        {
            geometry.vertices.push(points[i]);
        }
    }

    //vertices (bool) - whether to display Voronoi vertices
    //edges (bool) - whether to display Voronoi edges
    //medial (bool) - whether to display only Voronoi edges that are inside polygon
    //Pts - outer polygon contour points, HolePts - self-explanatory
   function VoronoiInit(Pts, vertices = true, edges = true, medial = false, HolePts = undefined)
    {
        //rhill voronoi

        voronoi = new Voronoi();

        if (vertices === false && edges === false)
        {
            vertices = true;
        }
        BR = BoundingRectangle(Pts);

        bbox = {
                    xl: BR.xLeft,
                    xr: BR.xRight,
                    yt: BR.yBottom,
                    yb: BR.yTop
                };

        var sites = [];

        for (let i = 0; i < voronoiBoundarySubdiv.length; i++)
        {
            sites.push({
                x: voronoiBoundarySubdiv[i].x,
                y: voronoiBoundarySubdiv[i].y
            });
        }

        diagram = voronoi.compute(sites, bbox);
        var polygonContour = ConcatenateContourPoints(polygonPoints);
        var polygonHole = (HolePts !== undefined ? ConcatenateContourPoints(HolePts) : undefined);

        if (vertices)
        {
            var voronoiVertices = [];

            for (let j = 0; j < diagram.vertices.length; j++)
            {
                voronoiVertices.push(new THREE.Vector2(diagram.vertices[j].x, diagram.vertices[j].y));
            }

            PushVertexGeometry(Vectors2ToVectors3(voronoiVertices, false, true), pointsVoronoiGeometry);
        }
        if (edges)
        {
            var newEdgeGeometry;
            var vA, vB;

            for (let j = 0; j < diagram.edges.length; j++)
            {
                newEdgeGeometry = new THREE.Geometry();

                vA = new THREE.Vector2(diagram.edges[j].va.x, diagram.edges[j].va.y);
                vB = new THREE.Vector2(diagram.edges[j].vb.x, diagram.edges[j].vb.y);

                if (polygonHole !== undefined &&
                    (PointInside(polygonContour, vA) && PointInside(polygonContour, vB) &&
                    !PointInside(polygonHole, vA) && !PointInside(polygonHole, vB)) && medial)
                {
                    PushVertexGeometry(Vectors2ToVectors3([vA, vB], false, true), newEdgeGeometry);

                    voronoiEdgeGeometries.push(newEdgeGeometry);
                }
                else if (polygonHole === undefined &&
                    (PointInside(polygonContour, vA) && PointInside(polygonContour, vB)) && medial)
                {
                    PushVertexGeometry(Vectors2ToVectors3([vA, vB], false, true), newEdgeGeometry);

                    voronoiEdgeGeometries.push(newEdgeGeometry);
                }
                else if (!medial)
                {
                    PushVertexGeometry(Vectors2ToVectors3([vA, vB], false, true), newEdgeGeometry);

                    voronoiEdgeGeometries.push(newEdgeGeometry);
                }
            }
        }
    }
}


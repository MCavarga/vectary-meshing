/*global someFunction THREE:true*/

function Area(Pts)
{
    var A = 0.0;

    for (var i = 0; i < Pts.length - 1; i++)
    {
        A += Pts[i].x * Pts[i + 1].y - Pts[i].y * Pts[i + 1].x;
    }

    A += Pts[Pts.length - 1].x * Pts[0].y - Pts[Pts.length - 1].y * Pts[0].x;

    return A * 0.5;
}

//takes an array of Vector2's and finds their bounding rectangle given by xLeft, xRight, yTop, yBottom
function BoundingRectangle (Points)
{
    var offset = 1;
    var xL = 100000000, xR = -100000000;
    var yT = -100000000, yB = 100000000;

    for (let i = 0; i < Points.length; i++)
    {
        if (Points[i].x < xL) xL = Points[i].x;
        if (Points[i].x > xR) xR = Points[i].x;
        if (Points[i].y > yT) yT = Points[i].y;
        if (Points[i].y < yB) yB = Points[i].y;
    }

    return {
        xLeft: xL - offset,
        xRight: xR + offset,
        yTop: yT + offset,
        yBottom: yB - offset
    };
}

function BPsToPoints (BPs)
{
    var Pts = [];

    for (let i = 0; i < BPs.length; i++) Pts.push(BPs[i].point);

    return Pts;
}

// distance of a point X to Contour
// returnEdgeId returns an array: [DistanceToContour, index of the closest edge or vertex, projectionPoint]

function DistanceToContour(X, Contour, returnEdgeId = false)
{
    var a, b, c, dist = 10000000;
    var returnDist;
    var projectionPoint;

    if (!returnEdgeId)
    {
        for (let i = 0; i < Contour.length; i++)
        {
            if (Contour[i].internalAngle > Math.PI)
            {
                dist = Contour[i].point.distanceTo(X);
            }
            else
            {
                a = Contour[i].point.y - Contour[Contour[i].iNext].point.y;
                b = Contour[Contour[i].iNext].point.x - Contour[i].point.x;
                c = Contour[i].point.x * (Contour[Contour[i].iNext].point.y - Contour[i].point.y) +
                    Contour[i].point.y * (Contour[i].point.x - Contour[Contour[i].iNext].point.x);

                dist = Math.abs(a * X.x + b * X.y + c) / Math.sqrt(a * a + b * b);

                projectionPoint = new THREE.Vector2(
                    (b * (b * X.y - a * X.x) - a * c) / (a * a + b * b),
                    (a * (a * X.x - b * X.y) - b * c) / (a * a + b * b));

                if (PointOnSegment(projectionPoint, Contour[i].point, Contour[Contour[i].iNext].point))
                {
                    returnDist = 0;

                    continue;
                }
            }

            if (returnDist === undefined)
            {
                returnDist = dist;
            }
            else if (returnDist !== undefined && dist < returnDist)
            {
                returnDist = dist;
            }
        }
    }
    else
    {
        for (let i = 0; i < Contour.length; i++)
        {
            if (Contour[i].internalAngle > Math.PI)
            {
                dist = Contour[i].point.distanceTo(X);
                projectionPoint = Contour[i].point;
            }
            else
            {
                a = Contour[i].point.y - Contour[Contour[i].iNext].point.y;
                b = Contour[Contour[i].iNext].point.x - Contour[i].point.x;
                c = Contour[i].point.x * (Contour[Contour[i].iNext].point.y - Contour[i].point.y) +
                    Contour[i].point.y * (Contour[i].point.x - Contour[Contour[i].iNext].point.x);

                dist = Math.abs(a * X.x + b * X.y + c) / Math.sqrt(a * a + b * b);

                projectionPoint = new THREE.Vector2(
                    (b * (b * X.y - a * X.x) - a * c) / (a * a + b * b),
                    (a * (a * X.x - b * X.y) - b * c) / (a * a + b * b));

                if (PointOnSegment(projectionPoint, Contour[i].point, Contour[Contour[i].iNext].point))
                {
                    returnDist = 0;

                    continue;
                }
            }

            if (returnDist === undefined)
            {
                returnDist = [dist, i, projectionPoint];
            }
            else if (returnDist !== undefined && dist < returnDist[0])
            {
                returnDist = [dist, i, projectionPoint];
            }
        }
    }

    return returnDist;
}

function equalsWithEpsilon(P0, P1, epsilon)
{
    if (P0.x < P1.x + epsilon && P0.x > P1.x - epsilon &&
        P0.y < P1.y + epsilon && P0.y > P1.y - epsilon) return true;

    return false;
}

//checks if segment between P0 and P1 intersects segment between P2 and P3
function intersect(P0, P1, P2, P3, internal = false)
{
    //if (P1.x === P3.x && P1.y === P3.y) return undefined;

    var a, b, c, d, e, f;
    var Direction01 = P1.clone().sub(P0);
    var Direction23 = P3.clone().sub(P2);

    //if (Direction01.length() === 0 || Direction23.length() === 0) return undefined;

    a = Direction01.y;
    b = -Direction01.x;
    c = P0.y * Direction01.x - P0.x * Direction01.y;
    d = Direction23.y;
    e = -Direction23.x;
    f = P2.y * Direction23.x - P2.x * Direction23.y;

    var lambda0, lambda1;
    var intersection;

    //not parallel
    if (a * e - b * d !== 0)
    {

        intersection = new THREE.Vector2(
           (b * f - c * e) / (a * e - b * d),
           (c * d - f * a) / (a * e - b * d));

        lambda0 = intersection.clone().sub(P0).dot(Direction01) / Direction01.lengthSq();
        lambda1 = intersection.clone().sub(P2).dot(Direction23) / Direction23.lengthSq();

        if (!internal && (lambda0 >= 0 && lambda0 < 1) && (lambda1 >= 0 && lambda1 < 1))
        {
            return intersection;
        }
        else if (internal && (lambda0 > 10 * Number.EPSILON && lambda0 < 1 - 10 * Number.EPSILON) &&
                             (lambda1 > 10 * Number.EPSILON && lambda1 < 1 - 10 * Number.EPSILON))
        {
            return intersection;
        }
    }

    return undefined;
}

function intersectContour(P0, P1, contour, internal = false)
{
    var intersections = [];
    var currentInters;
    var N = contour.length;

    for (let i = 0; i < N; i++)
    {
        currentInters = intersect(P0, P1, contour[i].point, contour[contour[i].iNext].point, internal);
        if (currentInters !== undefined) intersections.push(currentInters);
    }

    return intersections;
}

function isClockWise(Pts)
{
    if (Area(Pts) > 0)
    {
        return false;
    }

    return true;
}

function isVisible(contour, iCurrent, iTarget)
{
    return !SegmentCrossesContour(contour, contour[iCurrent].point, contour[iTarget].point);
}

function mergePoints2D (points, precisionPoints = 5)
{
    var pointsMap = {};
    var unique = [];
    var changes = [];
    var P, key;
    var precision = Math.pow(10, precisionPoints);

    var N = points.length;

    for (let i = 0; i < N; i++)
    {
        P = points[i];
        key = Math.round(P.x * precision) + '_' + Math.round(P.y * precision);

        if (pointsMap[key] === undefined)
        {
            pointsMap[key] = i;
            unique.push(P);
            changes[i] = unique.length - 1;
        }
        else
        {
            changes[i] = changes[pointsMap[key]];
        }
    }

    return unique;
}

//finds the index and the projection point of the closest contour edge and returns a number between 0 and 1 corresponding to the orientation
function OrientationParam (X, Contour)
{
    var dist = DistanceToContour(X, Contour, true);
    var I = dist[1];
    var projectionPoint = dist[2];
    var lambda = projectionPoint.clone().sub(Contour[I].point).dot(Contour[Contour[I].iNext].point.clone().sub(Contour[I].point)) /
                 (Contour[Contour[I].iNext].point.clone().sub(Contour[I].point)).lengthSq();

    return (I + lambda) / Contour.length;
}

function PointInside(contour, X)
{
    var xMax = 100000;
    var N = contour.length;
    var intersectionCount = 0;

    var rayDirectionX = new THREE.Vector2(1 + 0.000000001 * Math.PI, 0.000000001 * Math.PI);
    var where = X.clone().addScaledVector(rayDirectionX, xMax);

    for (let i = 0; i < N; i++)
    {
        //treatment for horizontal edges
        if (contour[i].point.y <= contour[contour[i].iNext].point.y + Number.EPSILON && contour[i].point.y >= contour[contour[i].iNext].point.y - Number.EPSILON)
        {
            //if point X lies on edge contour[i] --- contour[contour[i].iNext]
            if (PointOnSegment(X, contour[i].point, contour[contour[i].iNext].point))
            {
                intersectionCount++;
            }
            else
            {
                continue;
            }
        }

        //upward edge
        if (contour[i].point.y < contour[contour[i].iNext].point.y)
        {
            //keep direction
            if (intersect(X, where, contour[i].point, contour[contour[i].iNext].point) !== undefined)
            {
                intersectionCount++;
            }
        }
        //downward edge
        else if (contour[i].point.y > contour[contour[i].iNext].point.y)
        {
            //flip direction
            if (intersect(X, where, contour[contour[i].iNext].point, contour[i].point) !== undefined)
            {
                intersectionCount++;
            }
        }
    }

    return (intersectionCount % 2 === 1);
}

//checks wheter point is in the interior of a line segment
function PointOnSegment(point, P0, P1)
{
    var a, b, c, distance;
    var Direction, lambda;

    a = P0.y - P1.y;
    b = P1.x - P0.x;
    c = P0.x * (P1.y - P0.y) + P0.y * (P0.x - P1.x);

    distance = Math.abs(a * point.x + b * point.y + c) / Math.sqrt(a * a + b * b);

    if (distance > Number.EPSILON) return false;

    Direction = P1.clone().sub(P0);

    lambda = (point.clone().sub(P0)).dot(Direction) / Direction.lengthSq();

    if (lambda >= 0 && lambda < 1)
    {
        return true;
    }

    return false;
}

//PointOnSegment extended to the whole contour
function PointOnContour(contour, point)
{
    var N = contour.length;

    for (let i = 0; i < N; i++)
    {
        if (PointOnSegment(point, contour[i].point, contour[contour[i].iNext].point))
        {
            return i;
        }
    }

    return undefined;
}

function PointReflect (point, lineDirection, lineFrom)
{
    var pointFromOrigin = point.clone().sub(lineFrom);
    var projection = lineFrom.clone().addScaledVector(lineDirection.normalize(),lineDirection.normalize().dot(pointFromOrigin));

    return point.clone().negate().addScaledVector(projection, 2);
}

function SegmentCrossesContour(contour, P0, P1)
{
    if (P0.x === P1.x && P0.y === P1.y) return false;

    var N = contour.length;
    var SplitPoint;
    var SplitPoints = [];

    SplitPoints.push(P0);

    for (let i = 0; i < N; i++)
    {
        SplitPoint = intersect(P0 , P1, contour[i].point, contour[contour[i].iNext].point);
        if (SplitPoint !== undefined &&
            ((SplitPoint.x === P1.x && SplitPoint.y === P1.y) || (P0.x === SplitPoint.x && P0.y === SplitPoint.y)))
        {
            SplitPoint = undefined;
            continue;
        }
        if (SplitPoint !== undefined)
        {
            SplitPoints.push(SplitPoint);
        }
    }

    SplitPoints.push(P1);

    function compareDistance(A, B)
    {
        if (A.distanceTo(P0) < B.distanceTo(P0)) return -1;

        if (A.distanceTo(P0) > B.distanceTo(P0)) return 1;

        return 0;
    }
    //sort by distance from P0
    SplitPoints.sort(compareDistance);
    //merge
    SplitPoints = mergePoints2D(SplitPoints);

    //will test whether consecutive intersections between P0 and P1 from SplitPoints queue lie inside polygon
    var CurrentIntersection = SplitPoints.shift();
    var NextIntersection = SplitPoints.shift();

    while (SplitPoints.length > 0)
    {
        if (!SegmentInside(contour, CurrentIntersection, NextIntersection))
        {
            return true;
        }
        CurrentIntersection = NextIntersection;
        NextIntersection = SplitPoints.shift();
    }

    if (!SegmentInside(contour, CurrentIntersection, NextIntersection))
    {
        return true;
    }

    return false;
}

function SegmentInside(contour, P0, P1)
{
    var N = contour.length;

    for (let i = 0; i < N; i++)
    {
        //segment P0--->P1 is an edge
        if (contour[i].point.x === P0.x && contour[i].point.y === P0.y &&
            contour[contour[i].iNext].point.x === P1.x && contour[contour[i].iNext].point.y === P1.y)
        {
            return true;
        }
    }

    var MidPoint = P0.clone().multiplyScalar(0.5).addScaledVector(P1, 0.5);

    return PointInside(contour, MidPoint);
}

//subdivides a polygonal contour based on elementSize
function Subdivide(Pts, elementSize)
{
    var newPoints = [];
    var subdivisions, edgeLength;
    var randVar = 0.0;

    var oldN = Pts.length;
    var P1, P2, Pnew, lambda;

    for (let i = 0; i < oldN; i++)
    {
        P1 = Pts[i];
        newPoints.push(P1);
        P2 = Pts[(i + 1) % oldN];
        edgeLength = P1.distanceTo(P2);
        if (elementSize > 0.5 * edgeLength)
        {
            subdivisions = (edgeLength / elementSize > 1 ? (Math.floor(edgeLength / elementSize)) : 1);
        }
        else
        {
            subdivisions = (edgeLength / elementSize > 1 ? Math.floor(edgeLength / elementSize) : 1);
        }
        for (let j = 0; j < subdivisions - 1; j++)
        {
            lambda = (j + 1) / subdivisions;
            Pnew = P1.clone().multiplyScalar(1 - lambda).addScaledVector(P2, lambda).add(new THREE.Vector2(2 * randVar * Math.random() - randVar, 2 * randVar * Math.random() - randVar));
            newPoints.push(Pnew);
        }
    }

    return newPoints;
}


//calculates the winding number of point X with respect to the polygonal contour
function WindingNumber (contour, X, direction)
{
    var wind = 0;
    var xMax = 100000000000;
    //default direction is (1, 0)
    var rayDirectionX = (direction === undefined ? new THREE.Vector2(1, 0) : direction);

    for (let i = 0; i < contour.lenght; i++)
    {
        if (contour[contour[i].iNext].point.y !== contour[i].point.y &&
            intersect(X, X.clone().addScaledVector(rayDirectionX, xMax), contour[i].point, contour[contour[i].iNext].point))
        {
            wind += Math.sign(contour[contour[i].iNext].point.y - contour[i].point.y);
        }
    }

    return wind;
}

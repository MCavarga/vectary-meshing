/*global someFunction THREE:true*/

function polygPoints()
{
    var polyhedronPts = [];
    //var position = new THREE.Vector2(Number.EPSILON * 1000 * Math.PI, Number.EPSILON * 1000 * Math.PI);
    var position = new THREE.Vector2(1000 * Number.EPSILON, 1000 * Number.EPSILON);
    //var position = new THREE.Vector2(0, 0);

    polyhedronPts.push(new THREE.Vector2(1, -1.5).add(position));
    polyhedronPts.push(new THREE.Vector2(0.5, -1));
    polyhedronPts.push(new THREE.Vector2(0.5, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(1, 1));
    polyhedronPts.push(new THREE.Vector2(1.5, 0.5).add(position));
    polyhedronPts.push(new THREE.Vector2(1.5, 1.5));

    polyhedronPts.push(new THREE.Vector2(0.0, 1.5));

    polyhedronPts.push(new THREE.Vector2(-1.5, 1.5).add(position));
    polyhedronPts.push(new THREE.Vector2(-1.5, 0.5));
    polyhedronPts.push(new THREE.Vector2(-1, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(-0.5, 1));
    polyhedronPts.push(new THREE.Vector2(-0.5, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(-1, -1.5));

    return polyhedronPts;
}

function polygPoints2()
{
    var polyhedronPts = [];
    var position = new THREE.Vector2(0, 0);

    polyhedronPts.push(new THREE.Vector2(0.75, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(1.5, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(0.7, 0).add(position));
    polyhedronPts.push(new THREE.Vector2(0.5, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(0, 1.1).add(position));
    polyhedronPts.push(new THREE.Vector2(-0.5, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(-1.5, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(-0.75, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(0, 0.5).add(position));

    return polyhedronPts;
}

function polygPoints3()
{
    var polyhedronPts = [];
    var position = new THREE.Vector2(0, 0);

    polyhedronPts.push(new THREE.Vector2(1, 0));
    polyhedronPts.push(new THREE.Vector2(1, 0.5));
    polyhedronPts.push(new THREE.Vector2(1, 1));
    polyhedronPts.push(new THREE.Vector2(-1, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(-1, 0).sub(position));

    return polyhedronPts;
}


function polygPoints4()
{
    var polyhedronPts = [];
    var position = new THREE.Vector2(0, 0.2);

    polyhedronPts.push(new THREE.Vector2(1, 0));
    polyhedronPts.push(new THREE.Vector2(0.5, 0.5));
    polyhedronPts.push(new THREE.Vector2(1, 1));
    polyhedronPts.push(new THREE.Vector2(0.5, 1.5));
    polyhedronPts.push(new THREE.Vector2(-0.3, 2));
    polyhedronPts.push(new THREE.Vector2(0, 2.5));
    polyhedronPts.push(new THREE.Vector2(1, 3));
    polyhedronPts.push(new THREE.Vector2(0.5, 3.5));
    polyhedronPts.push(new THREE.Vector2(1, 4));
    polyhedronPts.push(new THREE.Vector2(-1, 4).add(position));
    polyhedronPts.push(new THREE.Vector2(-1, 0).sub(position));

    return polyhedronPts;
}

function polygPointsHole1()
{
    var polyhedronPts = [];
    var holes = [];
    var position = new THREE.Vector2(0, 0);

    polyhedronPts.push(new THREE.Vector2(0, -1).add(position));
    polyhedronPts.push(new THREE.Vector2(1, 0).add(position));
    polyhedronPts.push(new THREE.Vector2(0, 1).add(position));
    polyhedronPts.push(new THREE.Vector2(-1, 0).add(position));

    holes.push([]);
    holes[0].push(new THREE.Vector2(0.25, -0.25).add(position));
    holes[0].push(new THREE.Vector2(0.25, 0.25).add(position));
    holes[0].push(new THREE.Vector2(-0.25, 0.25).add(position));
    holes[0].push(new THREE.Vector2(-0.25, -0.25).add(position));

    return [polyhedronPts, holes];
}


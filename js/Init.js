/*global someFunction THREE:true*/
/*global someFunction ObjectsToScene:true*/

//global vars:
var scene, camera, lights, renderer, orbit, gui;

init();
render();

function init()
{
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

	renderer = new THREE.WebGLRenderer({antialias: true});
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(0x000000, 1);
	document.body.appendChild(renderer.domElement);

	orbit = new THREE.OrbitControls(camera, renderer.domElement);

	// default arguments: (scene), AdvancingFront = true, quads = false, vertexLabels = false
	ObjectsToScene(scene, true);

	camera.position.z = 3;

	lights = [];
	lights[0] = new THREE.PointLight(0xffffff, 1, 0);
	lights[1] = new THREE.PointLight(0xffffff, 1, 0);
	lights[2] = new THREE.PointLight(0xffffff, 1, 0);

	lights[0].position.set(0, 200, 0);
	lights[1].position.set(100, 200, 100);
	lights[2].position.set(-100,-200,-100);

	scene.add(lights[0]);
	scene.add(lights[1]);
	scene.add(lights[2]);

	window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize()
{
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}

function render()
{
	requestAnimationFrame(render);
	renderer.render(scene, camera);
}

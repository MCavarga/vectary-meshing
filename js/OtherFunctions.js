
function Subdivide(Pts, elementSize)
{
    var newPoints = [];
    var subdivisions, edgeLength;

    var oldN = Pts.length;
    var P1, P2, Pnew, lambda;

    for (let i = 0; i < oldN - 1; i++)
    {
        P1 = Pts[i];
        newPoints.push(P1);
        P2 = Pts[i + 1];
        edgeLength = P1.distanceTo(P2);
        subdivisions = (edgeLength / elementSize > 1 ? Math.floor(edgeLength / elementSize) : 1);
        for (let j = 0; j < subdivisions - 1; j++)
        {
            lambda = (j + 1) / subdivisions;
            Pnew = P1.clone().multiplyScalar(1 - lambda).addScaledVector(P2, lambda);
            newPoints.push(Pnew);
        }
    }
    P1 = Pts[oldN - 1];
    newPoints.push(P1);
    P2 = Pts[0];
    for (let j = 0; j < subdivisions - 1; j++)
    {
        lambda = (j + 1) / subdivisions;
        Pnew = P1.clone().multiplyScalar(1 - lambda).addScaledVector(P2, lambda);
        newPoints.push(Pnew);
    }

    return newPoints;
}

//custom Triangulate
function EarClippingTriangulate(geometry, polyPoints)
{
    var position = new THREE.Vector2(0, 3);

    var Pts = [];

    if (isClockWise(polyPoints) === false)
    {
        console.log("Polygon is anti-clockwise");
        for (let i = 0; i < polyPoints.length; i++)
        {
            Pts.push(polyPoints[i].clone().add(position));
            geometry.vertices.push(polyPoints[i].clone().add(position));
        }
    }
    else
    {
        console.log("Polygon is clockwise");
        for (let i = polyPoints.length - 1; i >= 0; i--)
        {
            Pts.push(polyPoints[i].clone().add(position));
            geometry.vertices.push(polyPoints[i].clone().add(position));
        }
    }

    var Neighbors = [];

    Neighbors.push([Pts.length - 1, 1]);
    for (let i = 1; i < Pts.length - 1; i++)
    {
        Neighbors.push([i - 1, i + 1]);
    }

    Neighbors.push([Pts.length - 2, 0]);

    for (let i = 0; i < Neighbors.length; i++)
    {
        console.log("Neighbors[",i,"] = (",Neighbors[i][0],",",Neighbors[i][1],")");
    }

    for (let i = 0; i < Neighbors.length - 1; i++)
    {
        console.log("P[",i,"].isAnEar() = ",isAnEar(Pts, Neighbors[i][0], i, Neighbors[i][1]));
        if (isAnEar(Pts, Neighbors[i][0], i, Neighbors[i][1]))
        {
            //if Pts[i] is an ear, a triangle will be added to the FaceBuffer.
            geometry.faces.push(new THREE.Face3(Neighbors[i][0], i, Neighbors[i][1]));
            //now we need to remove this vertex from the list of neighbors
            if (i === 0)
            {
                Neighbors[i + 1][0] = Neighbors[i][0];
                Neighbors[Neighbors.length - 1][1] = Neighbors[i][1];
            }
            else
            {
                Neighbors[i + 1][0] = Neighbors[i][0];
                Neighbors[i - 1][1] = Neighbors[i][1];
            }
        }
    }

    console.log("------------------------------------------------");
    for (let i = 0; i < Neighbors.length; i++)
    {
        console.log("Neighbors[",i,"] = (",Neighbors[i][0],",",Neighbors[i][1],")");
    }

    geometry.computeFaceNormals();
}

function isAnEar(Pts, iPrev, iCurrent, iNext)
{
    var P;
    var A, B, C;

    A = Pts[iPrev];
    B = Pts[iCurrent];
    C = Pts[iNext];

    var Origin = new THREE.Vector2();

    if (Area([A, B, C]) <= Number.EPSILON)
    {
        return false;
    }

    for (let i = 0; i < Pts.length; i++)
    {
        P = Pts[i];

        if (P.clone().sub(A).equals(Origin) || P.clone().sub(B).equals(Origin) || P.clone().sub(C).equals(Origin)) continue;

        if (Area([A, B, P]) > 0 && Area([B, C, P]) > 0 && Area([C, A, P]) > 0)
        {
            return false;
        }
    }

    return true;
}


        //concave angle, i.e.: another reentrant vertex
 /*if (contour[i].internalAngle > Math.PI && isVisible(contour, iCurrent, i))
        {
            console.log("vertex[",iCurrent,"] with vertex[",i,"]");
            offset = 0.5 * contour[iCurrent].point.distanceTo(contour[i].point);
            console.log("offset = 0.5 * d(contour[",iCurrent,"], contour[",i,"]) = ", offset);

            Direction = contour[i].point.clone().sub(contour[iCurrent].point).normalize();
            console.log("Direction = (",Direction.x,",",Direction.y,")");

            BranchPoint = contour[iCurrent].point.clone().addScaledVector(Direction, offset);
                console.log("AdmissableInitialBP = (",BranchPoint.x,",",BranchPoint.y,")");
            if (PointInside(contour, BranchPoint))
            {
                AdmissableBranchPoints.push(BranchPoint);
            }

            if (lastOffset === undefined || (lastOffset !== undefined && offset < lastOffset))
            {
                lastOffset = offset;
            }
        }

        console.log(" --- SegmentCrossesContour: ");
    console.log("SplitPoints.length = ", SplitPoints.length);

    for (let i = 0; i < SplitPoints.length; i++)
    {
        console.log("SplitPoint [",i,"] = (",SplitPoints[i].x,",",SplitPoints[i].y,")");
    }

    //will test whether consecutive intersections between P0 and P1 from SplitPoints queue lie inside polygon
    var CurrentIntersection = SplitPoints.shift();
    var NextIntersection = SplitPoints.shift();

    while (SplitPoints.length > 0)
    {
        if (!SegmentInside(contour, CurrentIntersection, NextIntersection))
        {
            return true;
        }
        CurrentIntersection = NextIntersection;
        NextIntersection = SplitPoints.shift();
    }

    if (!SegmentInside(contour, CurrentIntersection, NextIntersection))
    {
        return true;
    }

    return false; */

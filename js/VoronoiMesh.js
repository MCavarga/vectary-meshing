/*global someFunction Area:true*/
/*global someFunction BoundingRectangle:true*/
/*global someFunction ConcatenateContourPoints:true*/
/*global someFunction Delaunay:true*/
/*global someFunction DistanceToContour:true*/
/*global someFunction intersect:true*/
/*global someFunction intersectContour:true*/
/*global someFunction isClockWise:true*/
/*global someFunction isVisible:true*/
/*global someFunction mergePoints2D:true*/
/*global someFunction OrientationParam:true*/
/*global someFunction PointInside:true*/
/*global someFunction PointOnContour:true*/
/*global someFunction PointOnSegment:true*/
/*global someFunction PointReflect:true*/
/*global someFunction PushVertexGeometry:true*/
/*global someFunction SegmentCrossesContour:true*/
/*global someFunction SegmentInside:true*/
/*global someFunction Subdivide:true*/
/*global someFunction THREE:true*/
/*global someFunction Voronoi:true*/
/*global someFunction WindingNumber:true*/

class Triangle1
{
    constructor (triplets, i, j, k)
    {
        this.i = i; this.j = j; this.k = k;
        this.neighborIndices = [];
        for (let x = 0; x < triplets.length; x++)
        {
            if (triplets[x][0] === i && triplets[x][1] === j && triplets[x][2] === k) continue;
            else if (triplets[x].includes(i) && triplets[x].includes(j) && !triplets[x].includes(k))
            {
                this.neighborIndices.push(x);
            }
            else if (triplets[x].includes(j) && triplets[x].includes(k) && !triplets[x].includes(i))
            {
                this.neighborIndices.push(x);
            }
            else if (triplets[x].includes(k) && triplets[x].includes(i) && !triplets[x].includes(j))
            {
                this.neighborIndices.push(x);
            }
        }
    }
}

function InANeighborhood (point, vertices, epsilon)
{
    var N = vertices.length;
    var distance;

    if (N === 0) return false;

    for (let i = 0; i < N; i++)
    {
        //unnecessary to calculate distance of points outside of the 2 * epsilon bounding rectangle
        if (Math.abs(vertices[i].x - point.x) > 2 * epsilon || Math.abs(vertices[i].y - point.y) > 2 * epsilon)
        {
            continue;
        }

        distance = point.distanceTo(vertices[i]);

        if (distance < epsilon) return true;
    }

    return false;
}

//returns a vertex index if the point equals the vertex within precisionPoints decimals
function IsAVertex (point, Contour)
{
    var precisionPoints = 6;
    var precision = Math.pow(10, precisionPoints);
    var key1, key2;

    key1 = Math.round(point.x * precision) + '_' + Math.round(point.y * precision);

    for (let i = 0; i < Contour.length; i++)
    {
        key2 = Math.round(Contour[i].point.x * precision) + '_' + Math.round(Contour[i].point.y * precision);
        if (key1 === key2) return i;
    }

    return undefined;
}

function VoronoiMesh (contour, diagram, elemSize, thresholdAngle = 0.7 * Math.PI)
{
    //squareness threshold
    var sqThreshold = 0.3;

    //because almost all geometric operations are written using a concatenated contour (doubly-linked list)
    var Contour = ConcatenateContourPoints(contour);

    //geometry
    var newGeom;
    var MeshElements = [];

    var MeshNodes = [];

    var currentSite, SitePoint;
    var vA, vB;
    var BoundaryNode, interiorVert, newNode, auxInters;

    var M = diagram.cells.length;
    var N = Contour.length;
    var nodeCount;
    var edgeLengths, edgeLength, medialDistance;
    var lambda, siteIdx, siteAngle;
    var normalDirection, auxNx, auxNy;

    var CellEdges;


    //iterating thrhough voronoi cells
    for (let i = 0; i < M; i++)
    {
        currentSite = diagram.cells[i].site;
        SitePoint = toThreejsPoint2D(currentSite);
        MeshNodes.push(SitePoint);

        siteIdx = IsAVertex(SitePoint, Contour);
        siteAngle = Contour[siteIdx].internalAngle;

        CellEdges = diagram.cells[i].halfedges;
        CellEdges = CellEdges.map((a) => a.edge);

        CellEdges.sort(compareLength);

        edgeLengths = [];
        for (let j = 0; j < CellEdges.length; j++)
        {
            edgeLengths.push(VEdgeLength(CellEdges[j]));
        }

        if (VEdgeLength(CellEdges[0]) >= 0.5 * elemSize && VEdgeLength(CellEdges[1]) >= 0.5 * elemSize)
        {
            for (let j = 0; j < 2; j++)
            {
                vA = toThreejsPoint2D(CellEdges[j].va);
                vB = toThreejsPoint2D(CellEdges[j].vb);
                //assuming there's at most one intersection
                BoundaryNode = intersectContour(vA, vB, Contour)[0];

                if (BoundaryNode === undefined)
                {
                    //console.log("BoundaryNode[",i,",",j,"] undefined!");
                    continue;
                }

                MeshNodes.push(BoundaryNode);

                if (PointInside(Contour, vA)) interiorVert = vA;
                else if (PointInside(Contour, vB)) interiorVert = vB;
                else continue;
                edgeLength = interiorVert.distanceTo(BoundaryNode);
                nodeCount = (2 * edgeLength / elemSize > 1 ? Math.floor(2 * edgeLength / elemSize) : 1);
                for (let k = 0; k < nodeCount - 1; k++)
                {
                    lambda = (k + 1) / nodeCount;
                    newNode = BoundaryNode.clone().multiplyScalar(1 - lambda).addScaledVector(interiorVert, lambda);
                    if (!InANeighborhood(newNode, MeshNodes, 0.45 * elemSize))
                    {
                        MeshNodes.push(newNode);
                    }
                }
            }


            //additional treatment for the voronoi regions of concave vertices
            if (siteAngle > Math.PI)
            {
                for (let j = 2; j < CellEdges.length; j++)
                {
                    if (!PointInside(Contour, toThreejsPoint2D(CellEdges[j].vb))) continue;

                    normalDirection = toThreejsPoint2D(CellEdges[j].vb).sub(SitePoint);

                    edgeLength = normalDirection.length();
                    nodeCount = (2 * edgeLength / elemSize > 1 ? Math.floor(2 * edgeLength / elemSize) : 1);
                    for (let k = 0; k < nodeCount - 1; k++)
                    {
                        lambda = (k + 1) / nodeCount;
                        newNode = SitePoint.clone().multiplyScalar(1 - lambda).addScaledVector(SitePoint.clone().add(normalDirection), lambda);
                        if (!InANeighborhood(newNode, MeshNodes, 0.3 * elemSize))
                        {
                            MeshNodes.push(newNode);
                        }
                    }
                }
            }
            //adding another axis of points in the direction of the medial axis approximation,
            //for convex vertices with angle larger than thresholdAngle
            else if (siteAngle === Math.PI || (siteAngle > thresholdAngle && siteAngle < Math.PI))
            {
                if ((siteAngle > thresholdAngle && siteAngle < Math.PI))
                {
                    normalDirection = (Contour[Contour[siteIdx].iPrev].point.clone().sub(Contour[siteIdx].point)).normalize().add(
                                      (Contour[Contour[siteIdx].iNext].point.clone().sub(Contour[siteIdx].point)).normalize()).normalize();
                }
                else
                {
                    normalDirection = (Contour[siteIdx].point.clone().sub(Contour[Contour[siteIdx].iPrev].point)).normalize();
                    auxNx = normalDirection.x; auxNy = normalDirection.y;
                    normalDirection.setX(-auxNy);
                    normalDirection.setY(auxNx);
                }

                for (let j = 2; j < CellEdges.length; j++)
                {
                    vA = toThreejsPoint2D(CellEdges[j].va);
                    vB = toThreejsPoint2D(CellEdges[j].vb);
                    auxInters = intersect(SitePoint, SitePoint.clone().addScaledVector(normalDirection, 10 * edgeLengths[0]), vA, vB);
                    if (auxInters !== undefined)
                    {
                        interiorVert = auxInters;
                        break;
                    }
                }

                if (interiorVert === undefined || !PointInside(Contour, interiorVert))
                {
                    console.log("Error! Cannot find medial distance for site[",i,"]!");
                    continue;
                }
                else
                {
                    medialDistance = SitePoint.distanceTo(interiorVert);
                    nodeCount = (2 * medialDistance / elemSize > 1 ? Math.floor(2 * medialDistance / elemSize) : 1);
                    for (let k = 0; k < nodeCount; k++)
                    {
                        lambda = (k + 1) / nodeCount;
                        newNode = SitePoint.clone().multiplyScalar(1 - lambda).addScaledVector(interiorVert, lambda);
                        if (!InANeighborhood(newNode, MeshNodes, 0.3 * elemSize))
                        {
                            MeshNodes.push(newNode);
                        }
                    }
                }
            }
        }
    }

    N = diagram.vertices.length;

    for (let i = 0; i < N; i++)
    {
        interiorVert = toThreejsPoint2D(diagram.vertices[i]);
        if (PointInside(Contour, interiorVert)) MeshNodes.push(interiorVert);
    }

    MeshNodes = mergePoints2D(MeshNodes, 2);

    //additional nodal point removal:

    var NewMeshNodes = [];

    for (let i = 0; i < MeshNodes.length; i++)
    {
        if (!InANeighborhood(MeshNodes[i], NewMeshNodes, 0.35 * elemSize))
        {
            NewMeshNodes.push(MeshNodes[i]);
        }
    }

    //NewMeshNodes.sort(compareOrientationParam);
    NewMeshNodes.sort(compareDistanceToContour);

    var nestedList = toNestedList2D(NewMeshNodes);
    var TriangleIndices = Delaunay.triangulate(nestedList);
    var OutputTriangleIds = [];
    var center;
    var vertices;

    M = TriangleIndices.length;

    //only triangles whose centers of mass lie inside are allowed
    for (let i = 0; i < TriangleIndices.length; i++)
    {
        vertices = [
            NewMeshNodes[TriangleIndices[i][0]],
            NewMeshNodes[TriangleIndices[i][1]],
            NewMeshNodes[TriangleIndices[i][2]]];

        center = new THREE.Vector2(1 / 3 * (vertices[0].x + vertices[1].x + vertices[2].x),
                                   1 / 3 * (vertices[0].y + vertices[1].y + vertices[2].y));

        if (PointInside(Contour, center))
        {
            OutputTriangleIds.push(TriangleIndices[i]);
        }
    }

    var Quads = TripletsToQuads(OutputTriangleIds.slice(), NewMeshNodes);

    //var Triangles = TripletsToTriangles(OutputTriangleIds);

    return [NewMeshNodes, OutputTriangleIds, Quads];

    //---- ADDITIONAL FUNCTIONS -----

    function compareDistanceToContour(pt0, pt1)
    {
        if (DistanceToContour(pt0, Contour) < DistanceToContour(pt1, Contour)) return -1;
        if (DistanceToContour(pt0, Contour) > DistanceToContour(pt1, Contour)) return 1;

        return 0;
    }

    //longest to shortest
    function compareLength (e0, e1)
    {
        if (toThreejsPoint2D(e0.va).distanceTo(toThreejsPoint2D(e0.vb)) <
            toThreejsPoint2D(e1.va).distanceTo(toThreejsPoint2D(e1.vb))) return 1;

        if (toThreejsPoint2D(e0.va).distanceTo(toThreejsPoint2D(e0.vb)) >
            toThreejsPoint2D(e1.va).distanceTo(toThreejsPoint2D(e1.vb))) return -1;

        return 0;
    }

    function compareOrientationParam(pt0, pt1)
    {
        if (OrientationParam(pt0, Contour) < OrientationParam(pt1, Contour)) return 1;
        if (OrientationParam(pt0, Contour) > OrientationParam(pt1, Contour)) return -1;

        return 0;
    }

    function toThreejsPoint2D (vorPt)
    {
        return new THREE.Vector2(vorPt.x, vorPt.y);
    }

    //to utilize Ironwallaby's Delaunay triangulation
    function toNestedList2D (Pts)
    {
        var result = [];

        for (let i = 0; i < Pts.length; i++)
        {
            if (Pts[i] !== undefined)
            {
                result.push([Pts[i].x, Pts[i].y]);
            }
        }

        return result;
    }

    function VEdgeLength (E)
    {
        return toThreejsPoint2D(E.va).distanceTo(toThreejsPoint2D(E.vb));
    }
}

function Split2Tri (currentTri, edgeId, mpLength)
{
    var outTriangles = [];

    switch (edgeId)
    {
        case 0:
            outTriangles.push([currentTri[0], mpLength, currentTri[2]]);
            outTriangles.push([mpLength, currentTri[1], currentTri[2]]);
            break;
        case 1:
            outTriangles.push([currentTri[0], currentTri[1], mpLength]);
            outTriangles.push([mpLength, currentTri[2], currentTri[0]]);
            break;
        case 2:
            outTriangles.push([currentTri[0], currentTri[1], mpLength]);
            outTriangles.push([mpLength, currentTri[1], currentTri[2]]);
            break;
        default:
            outTriangles.push([currentTri[0], currentTri[1], currentTri[2]]);
    }

    return outTriangles;
}

function Squareness (V0, V1, V2, V3)
{
    //edges
    var E0 = V1.clone().sub(V0);
    var E1 = V2.clone().sub(V1);
    var E2 = V2.clone().sub(V3);
    var E3 = V3.clone().sub(V0);

    var parallelness02 = E0.dot(E2) / (E0.length() * E2.length());
    var parallelness13 = E1.dot(E3) / (E1.length() * E3.length());

    //diagonals
    var D02 = V2.clone().sub(V0);
    var D13 = V3.clone().sub(V1);
    //diagonal ratio
    var dRatio = Math.min(D02.length() / D13.length() , D13.length() / D02.length());

    return (1 / 3) * (parallelness02 + parallelness13 + dRatio);
}

function TripletsToTriangles (triplets)
{
    var triangles = [];

    for (let j = 0; j < triplets.length; j++)
    {
        triangles.push(new Triangle1(triplets, triplets[j][0], triplets[j][1], triplets[j][2]));
    }

    return triangles;
}

function TripletsToQuads (triplets, vertices)
{
    var quads = [], quadCandidates;
    var currentTriplet;
    var id0, id1, id2, id3;
    var triangle0, V0, V1, V2, V3, unsharedId;

    while (triplets.length > 0)
    {
        currentTriplet = triplets.pop();
        triangle0 = new Triangle1(triplets, currentTriplet[0], currentTriplet[1], currentTriplet[2]);
        if (triangle0.neighborIndices.length > 0)
        {
            quadCandidates = [];
            for (let j = 0; j < triangle0.neighborIndices.length; j++)
            {
                for (let k = 0; k < 3; k++)
                {
                    if (triplets[triangle0.neighborIndices[j]][k] !== triangle0.i &&
                        triplets[triangle0.neighborIndices[j]][k] !== triangle0.j &&
                        triplets[triangle0.neighborIndices[j]][k] !== triangle0.k)
                    {
                        unsharedId = triplets[triangle0.neighborIndices[j]][k];
                    }
                }

                //here I need to ensure the proper orientation of the new quad candidate with indices i, j, k, l

                if (triplets[triangle0.neighborIndices[j]].includes(triangle0.i) &&
                    triplets[triangle0.neighborIndices[j]].includes(triangle0.j))
                {
                    id0 = triangle0.i; id1 = unsharedId; id2 = triangle0.j; id3 = triangle0.k;
                }
                else if (triplets[triangle0.neighborIndices[j]].includes(triangle0.j) &&
                         triplets[triangle0.neighborIndices[j]].includes(triangle0.k))
                {
                    id0 = triangle0.i; id1 = triangle0.j; id2 = unsharedId; id3 = triangle0.k;
                }
                else if (triplets[triangle0.neighborIndices[j]].includes(triangle0.k) &&
                         triplets[triangle0.neighborIndices[j]].includes(triangle0.i))
                {
                    id0 = triangle0.i; id1 = triangle0.j; id2 = triangle0.k; id3 = unsharedId;
                }

                V0 = vertices[id0];
                V1 = vertices[id1];
                V2 = vertices[id2];
                V3 = vertices[id3];

                quadCandidates.push({
                                i: id0,
                                j: id1,
                                k: id2,
                                l: id3,
                                partnerId: triangle0.neighborIndices[j],
                                squareness: Squareness(V0, V1, V2, V3)
                                });
            }
            quadCandidates.sort(CompareSquareness);
            if (quadCandidates[0].squareness < 0.3)
            {
                continue;
            }
            quads.push(quadCandidates[0]);
            //now used triangles have to be removed
            triplets.splice(quadCandidates[0].partnerId, 1);
        }
        else
        {
            //a simple triangle
            quads.push({
                i: currentTriplet[0],
                j: currentTriplet[1],
                k: currentTriplet[2],
                squareness: 0.
            });
        }
    }

    return quads;

    function CompareSquareness (q1, q2)
    {
        if (q1.squareness < q2.squareness) return 1;
        if (q1.squareness > q2.squareness) return -1;

        return 0;
    }
}

